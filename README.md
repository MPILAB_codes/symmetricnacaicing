# SymmetricNACAIcing



## Getting started
This code is developed to add a pre-defined type of ice to a NACA airfoil or RG15 airfoil. Note, in the present version, only symmetric ice can be applied to an airfoil. Any additional anti-symmetry would need to be coded. This code was modified from the original code by Dirk Gorissen <dgorissen@gmail.com>.

This code is developed for the work:
Nath et al. "Parametric investigation of aerodynamic performance degradation due to icing on a symmetric airfoil" (2024)

The current icing implementations are:
- Glaze
- Horm
- Rime


Usage:
Here is a typical input:
\begin{verbatim}
python naca.py -p 0012 -n 400 -s  -th 0.023 -dep 0.15 -t 0.05 -i -d
\end{verbatim}
where -s assures greater clustering of points near trailing/leading edges. The icing should be turned on using -i flag. With the -i, we need to set the thickness (-th), depth (-de), and transition length (-t). We can either display the profiles (-d) or output the points

'''
python naca.py -p 0012 -n 1000 -s  -th 0.005 -dep 0.0025  -i horn  >log_horn_005
python naca.py -p 0012 -n 1000 -s  -th 0.0075 -dep 0.0025  -i horn  >log_horn_0075
python naca.py -p 0012 -n 1000 -s  -th 0.01 -dep 0.005  -i horn >log_horn_01
python naca.py -p 0012 -n 1000 -s  -th 0.015 -dep 0.005  -i horn >log_horn_015
python naca.py -p 0012 -n 1000 -s  -th 0.025 -dep 0.00833  -i horn  >log_horn_025
python naca.py -p 0012 -n 1000 -s  -th 0.05 -dep 0.01667  -i horn  >log_horn_05 
'''
